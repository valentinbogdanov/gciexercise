package com.portal.app.tests;

import static org.junit.Assert.assertTrue;

import com.portal.app.AbstractTest;
import com.portal.app.RetryAnalyzerImpl;
import com.portal.app.Exercise.Pages.PageObject;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.openqa.selenium.WebElement;

@RunWith(Suite.class)
@SuiteClasses({})
public class AllTests extends AbstractTest {

  public final String SPORTS_CORAL_URL = "http://sports.coral.co.uk/";

  protected Logger log = LogManager.getLogger(com.portal.app.Exercise.Pages.PageObject.class.getName());

  @org.testng.annotations.Test(retryAnalyzer = RetryAnalyzerImpl.class)
  public void addBetToBetslip() throws Exception {

    PageObject po = new PageObject();

    po.openPage(SPORTS_CORAL_URL);
    assertTrue(po.isCoral());
    po.navigateToFootball();

    WebElement placedBet = po.placeBet();
//    assertTrue(po.isBetSameAsPlacedBet(placedBet));

    po.splashClose();
  }

  @org.testng.annotations.Test(retryAnalyzer = RetryAnalyzerImpl.class)
  public void logInLogOut() {

    PageObject po = new PageObject();

    po.openPage("https://bet.coral.co.uk/#/");
    assertTrue(po.isCoral());
    po.login();
    assertTrue(po.isLoggedIn());
    po.logout();
    assertTrue(po.isLoggedOut());
  }
}

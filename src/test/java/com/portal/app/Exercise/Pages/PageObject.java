package com.portal.app.Exercise.Pages;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import com.portal.app.AbstractPage;
import com.portal.app.webdriver.WebdriverFactory;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Random;

public class PageObject extends AbstractPage {

  public final String BET_ID_PATTERN = "bet-price-";
  public final String USERNAME = "uatbettingauto1";
  public final String PASSWORD = "example";

  // HomePage
  @FindBy(css = ".logo")
  private WebElement SITE_LOGO;
  @FindBy(css = "#top-login-form .login-button")
  private WebElement HEADER_LOGIN_BUTTON;
  @FindBy(css = "#top-login-form .join-button")
  private WebElement HEADER_JOIN_BUTTON;
  @FindBy(xpath = "html/body/div[4]/div[2]/div/div/div[1]/div/div[2]/div[2]/div/div/ul/li[2]/a")
  private WebElement footballLink;
  @FindBy(xpath = "//*[@id=\"header\"]/div/ul/li[1]/a")
  private WebElement joinButtonMobile;
  @FindBy(xpath = "//*[@id=\"header\"]/div/ul/li[2]/a")
  private WebElement loginButtonMobile;
  @FindBy(id = "top-login")
  private WebElement topLogin;
  @FindBy(id = "top-password")
  private WebElement topPassword;
  @FindBy(xpath = "html/body/div[4]/div[1]/div[2]/div/div[2]/div/div/div[1]/div[1]")
  private WebElement userInfo;
  @FindBy(xpath = "//*[@id=\"fontWrapper\"]/div[1]/div[2]/div/div[2]/div/div/div[2]/div[2]/a[3]")
  private WebElement logoutLink;
  @FindBy(xpath = "//*[@id=\"login\"]/div[2]/button/span/strong")
  private WebElement loginButtonAfterLogout;

  protected Logger log = LogManager.getLogger(com.portal.app.Exercise.Pages.PageObject.class.getName());

  private final String logedInCookieName = "pas[galabingo][real][isOnline]";

  protected WebDriverWait wait = new WebDriverWait(driver, 30);

  public PageObject() {
    PageFactory.initElements(driver, this);
  }

  public PageObject(boolean isMobile) throws MalformedURLException {
    if (isMobile) {
      WebdriverFactory factory = new WebdriverFactory();
      WebDriver driver =
          factory.createDriver("iPhone 6", "orientation", "deviceType", "launch", "platform", "scope", "nativeProduct");
      PageFactory.initElements(driver, this);
    } else {
      PageFactory.initElements(driver, this);
    }
  }

  public void openPage() {
    super.goTo("http://www.coral.co.uk");
  }

  public void openPage(String url) {
    super.goTo(url);
  }

  public Boolean isCoral() {
    log.info("Checking if Login and Join Buttons are present");
    try {
      wait.until(ExpectedConditions.visibilityOf(HEADER_LOGIN_BUTTON));
      Assert.assertTrue(HEADER_JOIN_BUTTON.isDisplayed(), "Login Button is there but Join Button not");
      ATUReports.add("LOGIN AND JOIN HEADER BUTTONS ARE PRESENT", LogAs.PASSED, null);
      return true;
    } catch (Exception ex) {
      log.info(ex);
      ATUReports.add("LOGIN AND JOIN HEADER BUTTONS ARE NOT PRESENT", LogAs.FAILED,
          new CaptureScreen(CaptureScreen.ScreenshotOf.BROWSER_PAGE));
      return false;
    }
  }

  public Boolean isCoralMobile() {
    log.info("Checking if Login and Join Buttons are present");
    try {
      wait.until(ExpectedConditions.visibilityOf(loginButtonMobile));
      Assert.assertTrue(joinButtonMobile.isDisplayed(), "Login Button is there but Join Button not");
      ATUReports.add("LOGIN AND JOIN HEADER BUTTONS ARE PRESENT", LogAs.PASSED, null);
      return true;
    } catch (Exception ex) {
      log.info(ex);
      ATUReports.add("LOGIN AND JOIN HEADER BUTTONS ARE NOT PRESENT", LogAs.FAILED,
          new CaptureScreen(CaptureScreen.ScreenshotOf.BROWSER_PAGE));
      return false;
    }
  }

  public void navigateToFootball() {
    log.info("Clicking on Football link");
    wait.until(ExpectedConditions.elementToBeClickable(footballLink));
    footballLink.click();
    ATUReports.add("CLICKED ON " + footballLink.getText(), LogAs.PASSED, null);
  }

  public void login() {
    log.info("Logging in");
    wait.until(ExpectedConditions.elementToBeClickable(topLogin));
    wait.until(ExpectedConditions.elementToBeClickable(topPassword));
    wait.until(ExpectedConditions.elementToBeClickable(HEADER_LOGIN_BUTTON));

    topLogin.sendKeys(USERNAME);
    topPassword.sendKeys(PASSWORD);
    HEADER_LOGIN_BUTTON.click();
    ATUReports.add("Logged in successfully ", LogAs.PASSED, null);
  }

  public boolean isLoggedIn() {
    log.info("Checking if user is logged in");
    try {
      wait.until(ExpectedConditions.elementToBeClickable(userInfo));
      log.info("User is logged in");
      ATUReports.add("User is logged in", LogAs.PASSED, null);
      return true;
    } catch (Exception e) {
      log.info(e);
      log.info("User is not logged in");
      ATUReports.add("User is not logged in", LogAs.FAILED, null);
      return false;
    }
  }

  public void logout() {
    log.info("Logging out");
    wait.until(ExpectedConditions.elementToBeClickable(logoutLink));
    logoutLink.click();
    ATUReports.add("Logged out successfully", LogAs.PASSED, null);
  }

  public boolean isLoggedOut() {
    log.info("Checking if Logout is successful");
    try {
      wait.until(ExpectedConditions.visibilityOf(loginButtonAfterLogout));
      ATUReports.add("Logout successful", LogAs.PASSED, null);
      return true;
    } catch (Exception ex) {
      log.info(ex);
      ATUReports.add("Logout unsuccessful", LogAs.FAILED,
          new CaptureScreen(CaptureScreen.ScreenshotOf.BROWSER_PAGE));
      return false;
    }
  }

  public void initializeIphoneDriver() throws MalformedURLException {
    WebdriverFactory factory = new WebdriverFactory();
    WebDriver driver =
        factory.createDriver("iPhone 6", "orientation", "deviceType", "launch", "platform", "scope", "nativeProduct");
    this.driver = driver;
  }

  public WebElement placeBet() {
    log.info("Clicking on a bet");

    WebElement randomElement;
    do {
      Random r = new java.util.Random();
      List<WebElement> links = driver.findElements(By.tagName("a"));

      randomElement = links.get(r.nextInt(links.size()));
    } while (isMatchingBetIdPattern(randomElement.getAttribute("id")));

    wait.until(ExpectedConditions.elementToBeClickable(randomElement));
    randomElement.click();

    log.info("Bet Placed: " + randomElement.getTagName() + randomElement.getText());
    return randomElement;
  }

  public void splashClose() {
    log.info("Closing Splash Page");
    SITE_LOGO.click();
    ATUReports.add("SPLASH PAGE LOGO CLICKED", LogAs.PASSED, null);
  }

  public boolean isMatchingBetIdPattern(String elementText) {
    if (elementText.length() < BET_ID_PATTERN.length()) {
      return false;
    }

    for (int i = 0; i < BET_ID_PATTERN.length(); i++) {
      if (BET_ID_PATTERN.charAt(i) != elementText.charAt(i)) {
        return false;
      }
    }
    return true;
  }

}
